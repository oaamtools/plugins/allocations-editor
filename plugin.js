export async function init(pluginAPI) {
  await pluginAPI.loadStylesheets(["./css/oaam-allocations-editor.css"]);
  pluginAPI.implement("xgee.models", {
    modelPath: pluginAPI.getPath() + "Allocations.editorModel",
  });

  return true;
}

export var meta = {
  id: "editor.oaam.allocations",
  description: "Editor for the Allocations Layer of OAAM",
  author: "Matthias Brunner",
  version: "0.1.0",
  requires: ["ecore", "editor"],
};
